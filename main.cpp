#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include "kdtree.h"

int main(int argc, char *argv[]) {
    // creates an empty KDTree
    KDTree* sample = new KDTree();
    // opens the file
    std::ifstream file;
    file.open("data/rhode-island.txt");
    // declares variables for the specific line, and the data to parse from the line
    std::string line, description;
    double latitude, longitude;
    unsigned int point1, point2;
    // to ensure file opened properly
    if (file.is_open()) {
        // takes a line, checks for regularity, parses the data, inserts into the tree
        while (std::getline(file, line)) {
            // try block to make sure the line reads properly
            try {
                // finds the delimiting whitespaces between lat, long, and desc
                point1 = line.find(" ");
                point2 = line.substr(point1 + 1).find(" ");
                // uses those to create substrings
                latitude = std::stod(line.substr(0, point1));
                longitude = std::stod(line.substr(point1 + 1, point2));
                description = line.substr(point1 + point2 + 1);
                // trim description whitespace
                // left trim
                description.erase(0, description.find_first_not_of(' '));
                // right trim
                description.erase(description.find_last_not_of(' ') + 1);
                // enters the strings into the tree
                sample->insert(latitude, longitude, description.c_str());
            } catch (const std::exception &e) {
                std::cerr << "\tcould not parse line: " << line << std::endl;
            }
        }
        // closes the file
        file.close();
    }
    // if file failed to open
    else {
        std::cout << "File did not open" << std::endl;
    }
    // queries the tree a few times with assertions
    assert(sample->printNeighbors(41.8090567, -71.4204243, 5, "cafe") == 26);
    assert(sample->printNeighbors(41.8090567, -71.4204243, 10, "grave_yard") == 22);
    assert(sample->printNeighbors(41.7944329, -71.6588504, 10, "crazy") == 1);
    // deletes the tree
    delete sample;
    return 0;
}