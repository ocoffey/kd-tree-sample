#ifndef __kdtree__
#define __kdtree__

#include "kdnode.h"

class KDTree {
    private:
        // root node
        KDNode* root;
        // nodes in the tree
        unsigned int size;
        // recursive method for destroying the tree
        void destroy(KDNode* p);
        // the number of dimensions 'k' that the nodes will be, starting with k=2
        static const unsigned int k = 2;
        // internal insert function
        // q is the root
        // boolean return to help windows with the insertion
        bool insert(KDNode* p, KDNode* q);
        // private printNeighbors method overload
        unsigned int printNeighbors(double lat, double lon, double rad, const char* filter, KDNode* current);
    public:
        // public printNeighbors method
        unsigned int printNeighbors(double lat, double lon, double rad, const char* filter);
        void insert(double lat, double lon, const char* desc);
        unsigned int getSize();
        KDTree();
        ~KDTree();
};

#endif