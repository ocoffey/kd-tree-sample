#include "kdtree.h"
#include <iostream>

// KDTree constructor
KDTree::KDTree() {
    // initialize the root to null, since the tree is empty
    root = NULL;
    // initilize the size to 0, since the tree is empty
    size = 0;
}

// KDTree destructor
KDTree::~KDTree() {
    // run the recursive destroy method, starting with the root node
    destroy(root);
}

void KDTree::destroy(KDNode* p) {
    // when we hit a null pointer
    if (!p) {
        // return to the previous frame
        return; 
    }
    // no 'else' needed, since the 'if' reading will return out
    // recursively destroy the left child
    destroy(p->left);
    // recursively destroy the right child
    destroy(p->right);
    // once all those children are destroyed, delete this node
    delete p;
    return;
}

// external insert method
void KDTree::insert(double lat, double lon, const char* desc) {
    // create a new node with the passed data
    KDNode* p = new KDNode(lat, lon, desc);
    // increment the size of the tree
    size++;
    // for empty tree
    if (!root) {
        // point the root to the new node
        root = p;
        // this node has depth 0, so no incrementation needed
        return;
    }
    // else need to appropriately insert into the tree
    // using our overloaded insert method
    insert(p, root);
    return;
}

// internal overloaded insert method
bool KDTree::insert(KDNode* p, KDNode* q) {
    // increment the depth of the passed node
    (p->depth)++;
    // when we hit a null pointer on q
    if (!q) {
        // let the previous frame know to insert p as the child
        return true;
    }
    // check to see if we should compare the latitude or longitude
    // if checking for longitude as opposed to lat, since less checks
    // should be required
    // when checking for longitude, this will give '1', which will be 'true' for the 'if'
    if ((q->depth)%2) {
        // recursively check to see if we should place this to the right
        // if new node's data is larger than q's
        if (p->data[1] > q->data[1]) {
            // then send it further to the right
            // using the methods return function as data for an insertion 'if'
            // if the next frame had an empty q->right
            if(insert(p, q->right)) {
                // then p becomes q->right
                q->right = p;
            }
        }
        // else if q's longitude is greater or equal to p's
        else {
            // try and insert to the left
            if (insert(p, q->left)) {
                q->left = p;
            }
        }
    }
    // else checking for latitude
    else {
        // if p has higher data, try and place to the right
        if (p->data[0] > q->data[0]) {
            // similar to before, only using different a different dimension
            if (insert(p, q->right)) {
                q->right = p;
            }
        }
        else {
            if (insert(p, q->left)) {
                q->left = p;
            }
        }
    }
    // to let the previous frames know when a node has been successfully placed
    return false;
}

// printNeighbors method
// used the passed latitude and longitude, along with a radius, to determine all the neighbors
// that match a given filter within the given radius
// calls an overloaded helper method for tracking the current node
unsigned int KDTree::printNeighbors(double lat, double lon, double rad, const char* filter) {
    // 
    std::cout << "Neighbors of central point: ";
    std::cout << lat << ", " << lon << "\n";
    unsigned int neighbors = printNeighbors(lat, lon, rad, filter, root);
    std::cout << "There were " << neighbors << " neighbors of the central point.\n";
    return neighbors;
}

unsigned int KDTree::printNeighbors(double lat, double lon, double rad, const char* filter, KDNode* current) {
    // returns to the previous frame upon nulls
    if (!current) {
        return 0;
    }
    // for keeping track of childrens neighbors
    unsigned int neighbors = 0;
    // compares if the distance is within the radius, using cityblock measurement
    if (current->distance(lat, lon) <= rad) {
        // if the current node also matches the filter
        if ((current->description).find(filter) != std::string::npos) {
            // print the coordinates, and the description of the location
            // precision set to maintain as best accuracy as possible
            std::cout.precision(9);
            std::cout << "[" << current->data[0] << ", " << current->data[1] << "] - " << current->description << "\n";
            // and increment the amount of neighbors found
            neighbors++;
        }
        // calls this method on both children
        neighbors += printNeighbors(lat, lon, rad, filter, current->left);
        neighbors += printNeighbors(lat, lon, rad, filter, current->right);
    }
    // else checks for longitudinal comparison
    else if ((current->depth)%2) {
        // if this nodes longitude is less than the center's longitude
        if (current->data[1] < lon) {
            // recurse right, to try and approach the central point
            neighbors += printNeighbors(lat, lon, rad, filter, current->right);
        }
        // else recurse left, to try and approach the central point
        else {
            neighbors += printNeighbors(lat, lon, rad, filter, current->left);
        }
    }
    // else latitudinal comparison
    else {
        // if this nodes latitude is less than the center's latitude
        if (current->data[0] < lat) {
            // recurse right
            neighbors += printNeighbors(lat, lon, rad, filter, current->right);
        }
        else {
            neighbors += printNeighbors(lat, lon, rad, filter, current->left);
        }
    }
    // return the number of neighbors collected to this frame
    return neighbors;
}