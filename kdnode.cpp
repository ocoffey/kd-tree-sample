// makes sure that kdnode isn't already defined/in use
#include "kdnode.h"
#include <math.h>
#include <iostream>

// KDNode constructor
KDNode::KDNode(double lat, double lon, const char* desc) {
    // initialize left and right children as null
    left = NULL;
    right = NULL;
    // initialize depth to 0, will increment as needed
    depth = 0;
    // assign the description passed to the method
    description = desc;
    // assign the data passed to the method
    data[0] = lat;
    data[1] = lon;
}

// KDNode descructor, no work needed
KDNode::~KDNode() {}

// internal distance method
double KDNode::distance(double lat, double lon) {
    // needed to degrees to radian conversion
    double param = M_PI / 180.0;
    // ~ radius of the Earth in miles (it varies because it's not a perfect sphere)
    double rad = 3956.0;
    // obtain distances
    double d_lat = abs(lat - data[0]) * param;
    double d_lon = abs(lon - data[1]) * param;
    // for cityblock distance, add then multiply by Earth radius
    return (d_lat + d_lon) * rad;
}