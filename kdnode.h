#ifndef __kdnode__
#define __kdnode_

#include <vector>
#include <string>

class KDNode {
    private:
        // the size of k
        unsigned const int size = 2;
        // data points k
        double data[2];
        // depth of the node
        unsigned int depth;
        // description for the datapoint
        std::string description;
        KDNode* left;
        KDNode* right;
        // distance method for calculations
        double distance(double lat, double lon);
    public:
        // insert the latitude, longitude, and description
        KDNode(double lat, double lon, const char* desc);
        ~KDNode();
        friend class KDTree;
};

#endif
