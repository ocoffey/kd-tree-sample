# kd-tree-sample

Create a functioning and efficient kd-tree, with functionality for printing neighbors within a given radius matching a particular keyword

## Steps

### Tree

1. Tree and Features
  * Grabs data from a file, maybe try GPS x/y coordinates
2. Insertion
3. Keyword and Radius based Search

### Testing

1. Locally